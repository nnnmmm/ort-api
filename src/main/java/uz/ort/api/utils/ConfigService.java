package uz.ort.api.utils;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Saidolim on 24.07.2017.
 *
 * @since 26.07.2018 17:55
 */
public class ConfigService {

    private static final Logger LOGGER = LoggerFactory.getLogger("ConfigService");

    private String filename;

    public ConfigService(String filename) {
        this.filename = filename;
    }

    public void readConfig(Vertx vertx, final Handler<AsyncResult<JsonObject>> aHandler) {
        // Read config and say after finish
        ConfigRetriever retriever = readFromFile(vertx);
        retriever.getConfig(
                ar -> {
                    if (ar.failed()) {
                        // Failed to retrieve the configuration
                        aHandler.handle(Future.failedFuture("Config not read"));
                    } else {
                        // Success
                        LOGGER.info("Config read: " + ar.result());
                        aHandler.handle(Future.succeededFuture(ar.result()));
                    }
                });
    }

    private ConfigRetriever readFromFile(Vertx vertx) {
        // Set file where to read configurations
        ConfigStoreOptions fileStore = new ConfigStoreOptions()
                .setType("file")
                .setFormat("json")
                .setConfig(new JsonObject().put("path", "./config/" + this.filename + ".json"));

        ConfigRetrieverOptions options = new ConfigRetrieverOptions()
                .addStore(fileStore);

        // Read config and say after finish
        return ConfigRetriever.create(vertx, options);
    }

    public static JsonObject getPostgreConf(JsonObject dbConfigFromFile) {
        return new JsonObject()
                .put("url", dbConfigFromFile.getString("url", "localhost"))
                .put("driver_class", dbConfigFromFile.getString("driver", ""))
                .put("max_pool_size", dbConfigFromFile.getInteger("maxPoolSize", 30))
                .put("user", dbConfigFromFile.getString("username", "username"))
                .put("password", dbConfigFromFile.getString("password", "password"));

    }
}