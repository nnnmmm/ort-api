package uz.ort.api.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.List;

/**
 * Created by Saidolim on 20.06.2017.
 */
public class JsonConverter {


    public static String toJson(Object object) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
        }
        return null;
    }

    public static JsonArray toJsonArr(Object objectList) {
        if (objectList instanceof List) {
            JsonArray arr = new JsonArray();
            if (((List) objectList).size() > 0) {
                for (Object obj : (List) objectList) {
                    String jsonItem = toJson(obj);
                    if (jsonItem != null)
                        arr.add(new JsonObject(jsonItem));
                }
            }
            return arr;
        }
        return null;
    }
}
